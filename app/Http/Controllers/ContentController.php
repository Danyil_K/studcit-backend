<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiResponse;
use App\Content;
use App\Page;
use App\Cover;
use Storage;
use Validator;
use App\Rules\CheckLng;

class ContentController extends Controller
{

    /**
     * create content /content/
    */
    public function createContent(Request $request) {

        $validateData = [
            'lang' => ['required', 'string', new CheckLng],
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:500',
            'page_id' => 'required|int'
        ];

        $validator = Validator::make($request->all(), $validateData);

        if($validator->fails()) return $this->sendError($validator->errors(), 400);

        # validation for url
        $lang = Content::checkLang($request->lang);

        # for relation with Pages
        $pageLang = Page::where('page_id', $request->page_id)->pluck('lang');

        # lang validation
        if ($lang === null) {
            return $this->sendError('There is no such language', 404);
        } else if ($lang !== $pageLang[0]) {
            return $this->sendError('Page language is different from content language', 404);
        }

        # created new Content
        $content = Content::create([
            'lang' => $lang,
            'title' => $request->title,
            'description' => $request->description,
            'page_id' => $request->page_id,
        ]);

        $image = null;
        # created new Cover to Content
        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('uploads', 'public');
            $image = 'storage/' . $image;
        }
        Cover::create([
            'image' => $image,
            'content_id' => $content->content_id
        ]);

        return $this->sendResponse($content, 'Created', 201);
    }


    /**
     * select content with /name/lang/id?
    */
    public function selectContent(Request $request, $lang = null, $page = null) {

        $lng = Content::pluck('lang');
        $data = [];
        foreach ($lng as $key => $value) {
            array_push($data, $value);
        }
        $groupLng = array_unique($data);

        $contents = array_reduce( $groupLng, function($akk, $key) {
            $content = Content::where('lang', $key)->get();
            foreach ($content as $k => $value) {
                $value->cover;
            }
            $akk[$key] = $content;
            return $akk;
        }, [] );

        $page = $request->page;
        $lang = $request->lang;

        if (!is_null($lang)) {
            $contents = Content::where('lang', $lang)->where('page_id', $page)->get();
            foreach ($contents as $key => $value) {
                $value->cover;
            }
        }

        return $this->sendResponse($contents, 'OK', 200);

    }


    /**
     * update model
    */
    public function updateContent(Request $request, $lang, $content_id) {

        # find model and update
        $content = Content::lang($lang)->findOrFail($content_id);
        $image = $content->cover['image'];  # image from coverTable id

        $validateData = [
            'lang' => ['required', 'string', new CheckLng],
            'title' => 'nullable|string|max:255',
            'description' => 'required|string|max:500',
            'page_id' => 'required|int',
            'image' => 'nullable'
        ];

        $dataToSave = [
            'lang' => $request->lang ? $request->lang : $content['lang'],
            'title' => $request->title ? $request->title : $content['title'],
            'description' => $request->description ? $request->description : $content['description'],
            'page_id' => $request->page_id ? $request->page_id : $content['page_id']
        ];

        if ($request->hasFile('image')) {
            if (Storage::disk('public')->exists($image) )  {
                Storage::disk('public')->delete($image);
            }
            $path = $request->file('image')->store('uploads', 'public');
            $image = 'storage/' . $path;
        }

        $validator = Validator::make($dataToSave, $validateData);
            if($validator->fails()) return $this->sendError($validator->errors(), 400);

        $content->cover['image'] = $image;
        $content->cover->save();

        $content->update($dataToSave);

        return $this->sendResponse($content, 'Updated', 201);

    }


    /**
     * delete model
    */
    public function deleteContent($contentId) {

        # find model
        $content = Content::find($contentId);

        # find image to Content model
        $image = $content->cover['image'];
            if (Storage::disk('public')->exists($image) ) Storage::disk('public')->delete($image);

        if ( is_null($content) ) {
            return $this->sendError('Not found', 404);
        } else {
            $content->delete();
        }

        return $this->sendResponse(null, 'Deleted', 204);

    }

    public function allContents(Request $request, $lang = null, $page = null) {

        $lng = Content::pluck('lang');
        $data = [];
        foreach ($lng as $key => $value) {
            array_push($data, $value);
        }
        $groupLng = array_unique($data);

        $contents = array_reduce( $groupLng, function($akk, $key) {
            $content = Content::where('lang', $key)->get();
            foreach ($content as $k => $value) {
                $value->cover;
            }
            $akk[$key] = $content;
            return $akk;
        }, [] );

        $page = $request->page;
        $lang = $request->lang;

        if (!is_null($lang)) {
            $contents = Content::where('lang', $lang)->where('page_id', $page)->get();
            foreach ($contents as $key => $value) {
                $value->cover;
            }
        }

        return $this->sendResponse($contents, 'OK', 200);

    }

}
