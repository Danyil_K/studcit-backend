<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiResponse;
use App\Contact;
use Validator;


class ContactController extends Controller
{

    public function getContact() {

        $contact = Contact::get();

        return $this->sendResponse($contact, 'OK', 200);

    }


    public function filterContact($lang) {

        $contact = Contact::lang($lang);

        if ( $contact === false || is_null($contact) ) {
            return $this->sendError('Not found', 404);
        } else {
            $contact = $contact->get();
        }

        return $this->sendResponse($contact, 'OK', 200);

    }


    public function updateContact(Request $request, $lang, $contact_id) {

        $validatedData = [
            'lang' => 'required|string',
            'email' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'street' => 'required|string|max:255',
            'office' => 'required|string|max:255'
        ];

        $contact = Contact::lang($lang)->findOrFail($contact_id);

        $dataToSave = [
            'lang' => $request->lang ? $request->lang : $contact['lang'],
            'email' => $request->email ? $request->email : $contact['email'],
            'phone' => $request->phone ? $request->phone : $contact['phone'],
            'street' => $request->street ? $request->street : $contact['street'],
            'office' => $request->office ? $request->office : $contact['office']
        ];

        $validator = Validator::make($dataToSave, $validatedData);

        if ( $validator->fails() )
            return $this->sendError($validator->errors(), 404);

        if ( $contact === false || is_null($contact) ) {
            return $this->sendError('Not found', 404);
        } else {
            $contact->update($dataToSave);
        }

        return $this->sendResponse($contact, 'Updated', 201);

    }

    # for Vadim
    public function allContacts() {

        $lng = Contact::pluck('lang');
        $data = [];
        foreach ($lng as $key => $value) {
            array_push($data, $value);
        }
        $groupLng = array_unique($data);

        $model = array_reduce( $groupLng, function($akk, $key) {
            $contact = Contact::where('lang', $key)->get();
            foreach ($contact as $k => $value) {
                $value->cover;
            }
            $akk[$key] = $contact;
            return $akk;
        }, [] );

        return $this->sendResponse($model, 'OK', 200);

    }

}
