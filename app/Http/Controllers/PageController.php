<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiResponse;
use App\Page;
use App\Cover;
use App\Content;
use App\Contact;
use Validator;
use App\Rules\CheckLng;


class PageController extends Controller
{

    public function createPage(Request $request) {

        $validateData = [
            'lang' => ['required', 'string', new CheckLng],
            'link' => 'required|string|max:255',
            'title' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:500',
            'image' => 'nullable',
            'video' => 'nullable'
        ];

        $validator = Validator::make($request->all(), $validateData);

        if($validator->fails()) return $this->sendError($validator->errors(), 400);

        # validation for url
        $lang = Page::checkLang($request->lang);

        # lang validation
        if ($lang === null) return $this->sendError('There is no such language', 404);

        # created new Page
        $page = Page::create([
            'lang' => $lang,
            'link' => $request->link,
            'title' => $request->title,
            'name' => $request->name,
            'description' => $request->description,
        ]);

        # created new Cover to Page
        if ( $request->hasFile('image') and $request->hasFile('video') ) {
            $image = $request->file('image')->store('uploads', 'public');
            $image = 'storage/' . $image;
            $video = $request->file('video')->store('uploads', 'public');
            $video = 'storage/' . $video;

            Cover::create([
                'image' => $image,
                'video' => $video,
                'page_id' => $page->page_id
            ]);
        }

        return $this->sendResponse($page, 'Created', 201);

    }

    /**
     * select all Pages with this lang
    */
    public function selectPages(Request $request, string $lang) {

        $pages = Page::lang($lang)->get();

        foreach ($pages as $key=>$value) {
            $value->cover;
        }

        return $this->sendResponse($pages, 'OK', 200);

    }

    /**
     * select one Page
    */
    public function selectPage(Request $request, string $lang, string $link) {

        $page = Page::lang($lang);

        if ($page === false) return $this->sendError('Not found', 404);

        if ($link !== null && $lang !== false)
            $page = $page->where('link', '=', $link)->first();
            $page->cover;   # with cover
            $page->content; # with content and cover

            $contact = Contact::lang($lang)->get()[0];

            $page->offsetSet('contact', $contact);  # add contacts

        return $this->sendResponse($page, 'OK', 200);

    }

    /**
     * update Page
    */
    public function updatePage(Request $request, string $lang, int $page_id) {

        # find model and update
        $page = Page::lang($lang)->findOrFail($page_id);
        $image = $page->cover['image'];  # image from coverTable id
        $video = $page->cover['video'];  # image from coverTable id

        $validateData = [
            'lang' => ['required', 'string', new CheckLng],
            // 'link' => 'required|string',
            'title' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:500',
            'image' => 'nullable',
            'video' => 'nullable'
        ];

        $dataToSave = [
            'lang' => $request->lang ? $request->lang : $page['lang'],
            // 'link' => $request->link ? $request->link : $page['link'],
            'title' => $request->title ? $request->title : $page['title'],
            'name' => $request->name ? $request->name : $page['name'],
            'description' => $request->description ? $request->description : $page['description'],
        ];

        if ($request->hasFile('image')) {
            if (Storage::disk('public')->exists($image) )  {
                Storage::disk('public')->delete($image);
            }
            $pathImg = $request->file('image')->store('uploads', 'public');
            $image = 'storage/' . $pathImg;
        }
        if ($request->hasFile('video')) {
            if (Storage::disk('public')->exists($video) )  {
                Storage::disk('public')->delete($video);
            }
            $pathVid = $request->file('video')->store('uploads', 'public');
            $video = 'storage/' . $pathVid;
        }

        $validator = Validator::make($dataToSave, $validateData);
            if($validator->fails()) return $this->sendError($validator->errors(), 400);

        $page->cover['image'] = $image;
        $page->cover['video'] = $video;
        $page->cover->save();

        $page->update($dataToSave);

        return $this->sendResponse($page, 'Updated', 201);

    }

    /**
     * delete Page
    */
    public function deletePage(int $page_id) {

        # find model
        $page = Page::find($page_id);

        # find image to Content model
        $image = $page->cover['image'];
        $video = $page->cover['video'];

        if (Storage::disk('public')->exists($image) ) {
            Storage::disk('public')->delete($image);
        }
        if (Storage::disk('public')->exists($video) ) {
            Storage::disk('public')->delete($video);
        }

        if ( is_null($page) ) {
            return $this->sendError('Not found', 404);
        } else {
            $page->delete();
        }

        return $this->sendResponse(null, 'Deleted', 204);

    }


    public function allPages(Request $request, $link = null, $lang = null) {

        $lng = Page::pluck('lang');
        $data = [];
        foreach ($lng as $key => $value) {
            array_push($data, $value);
        }
        $groupLng = array_unique($data);

        $model = array_reduce( $groupLng, function($akk, $key) {
            $pages = Page::where('lang', $key)->get();
            foreach ($pages as $k => $value) {
                $value->cover;
            }
            $akk[$key] = $pages;
            return $akk;
        }, [] );

        $link = $request->link;
        $lang = $request->lang;
        if (!is_null($link) || !is_null($lang)) {
            $model = $model[$lang]->where('link', $link)->first();
        }

        return $this->sendResponse($model, 'OK', 200);

    }

}
