<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cover extends Model
{
    protected $table = 'cover';

    protected $primaryKey = 'cover_id';

    protected $fillable = [
        'cover_id',
        'image',
        'video',
        'page_id',
        'content_id'
    ];


    /**
     * for relations
    */
    public function page() {
        return $this->belongsTo('App\Page', 'page_id');
    }


    /**
     * for relations
    */
    public function content() {
        return $this->belongsTo('App\Content', 'content_id');
    }

}
