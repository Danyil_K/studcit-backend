<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';

    protected $primaryKey = 'contact_id';

    protected $fillable = [
        'contact_id',
        'lang',
        'email',
        'phone',
        'street',
        'office'
    ];


    public function scopeLang($query, $lang) {

        if ($lang === 'ua') {
            return $query->where('lang', 'ua');
        } else if ($lang === 'ru') {
            return $query->where('lang', 'ru');
        } else if ($lang === 'en') {
            return $query->where('lang', 'en');
        } else {
            return false;
        }

    }


}
