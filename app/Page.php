<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use App\Content;

class Page extends Model
{
    protected $table = 'page';


    protected $primaryKey = 'page_id';


    protected $fillable = [
        'page_id',
        'lang',
        'link',
        'title',
        'name',
        'description'
    ];


    /**
     * for getLanguage
    */
    public function scopeLang($query, $lang) {

        if ($lang === 'ua') {
            return $query->where('page.lang', 'ua');
        } else if ($lang === 'ru') {
            return $query->where('page.lang', 'ru');
        } else if ($lang === 'en') {
            return $query->where('page.lang', 'en');
        } else {
            return false;
        }

    }


    /**
     * check language. If has return str.
    */
    public static function checkLang($value) {
        if ($value === 'ua' || $value === 'ru' || $value === 'en') {
            return $value;
        } else {
            return null;
        }
    }


  /**
     * for relations page with cover
    */
    public function cover() {
        return $this->hasOne('App\Cover', 'page_id');
    }


    /**
     * for relations page with content
    */
    public function content() {
        return $this->hasMany('App\Content', 'page_id')->with('cover');
    }


}
