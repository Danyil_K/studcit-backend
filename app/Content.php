<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'content';

    protected $primaryKey = 'content_id';

    protected $fillable = [
       'content_id',
       'lang',
       'title',
       'description',
       'page_id'
    ];


    /**
     * for getLanguage
    */
    public function scopeLang($query, $lang) {

        if ($lang === 'ua') {
            return $query->where('content.lang', 'ua');
        } else if ($lang === 'ru') {
            return $query->where('content.lang', 'ru');
        } else if ($lang === 'en') {
            return $query->where('content.lang', 'en');
        } else {
            return false;
        }

    }


    /**
     * check language. If has return str.
    */
    public static function checkLang($value) {
        if ($value === 'ua' || $value === 'ru' || $value === 'en') {
            return $value;
        } else {
            return null;
        }
    }


    /**
     * for relations
    */
    public function page() {
        return $this->belongsTo('App\Page', 'page_id');
    }

    /**
     * for relations
    */
    public function cover() {
        return $this->hasOne('App\Cover', 'content_id');
    }

}
