<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


    # Frontend
    Route::get('/page/{link?}/{lang?}', 'PageController@allPages');   # all pages all lang
    Route::get('/content/{lang?}/{page?}', 'ContentController@allContents');  # allContents
    Route::get('/contact', 'ContactController@allContacts');  # allContents


    # Logging
    Route::prefix('auth')->group(function () {
        Route::post('login', 'UserController@login');
    });

    # Admin
    Route::group(['prefix' => 'admin', 'middleware' => 'auth:api'], function() {

        # registration/logout and check info user
        Route::post('signup', 'UserController@signup');
        Route::get('logout', 'UserController@logout');
        Route::get('user', 'UserController@user');

        # Pages
        Route::prefix('/page')->group(function(){
            Route::post('/', 'PageController@createPage');
            Route::get('/{lang}', 'PageController@selectPages');
            Route::get('/{lang}/{link}', 'PageController@selectPage');
            Route::post('/{lang}/{page_id}', 'PageController@updatePage')->where(['page_id' => '[0-9]+']);
            Route::delete('/{page_id}', 'PageController@deletePage')->where(['page_id' => '[0-9]+']);
        });

        # Content (posts)
        Route::prefix('/content')->group(function(){
            Route::post('/', 'ContentController@createContent');
            Route::get('/{lang?}/{page?}', 'ContentController@selectContent');
            Route::post('/{lang}/{content_id}', 'ContentController@updateContent');
            Route::delete('/{contentId}', 'ContentController@deleteContent')->where(['content_id' => '[0-9]+']);
        });

        # Contact information
        Route::prefix('/contact')->group(function(){
            Route::get('/', 'ContactController@getContact');
            Route::get('/{lang}', 'ContactController@filterContact');
            Route::put('/{lang}/{contact_id}', 'ContactController@updateContact')->where(['contact_id' => '[0-9]+']);
        });

    });

