<?php

use Illuminate\Database\Seeder;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* About page */
        DB::table('page')->insert([
            'lang' => 'ua',
            'link' => 'about',
            'title' => 'Про нас',
            'name' => 'Студентський Центр Інформаційних Технологій.',
            'description' => 'Ми можемо зробити цей світ краще.'
        ]);

        DB::table('page')->insert([
            'lang' => 'ru',
            'link' => 'about',
            'title' => 'О нас',
            'name' => 'Студенческий Центр Информационных Технологий.',
            'description' => 'Мы можем сделать этот мир лучше.'
        ]);

        DB::table('page')->insert([
            'lang' => 'en',
            'link' => 'about',
            'title' => 'About Us',
            'name' => 'Student Center of Information Technologies.',
            'description' => 'We can make this world better.'
        ]);


        /* Offers page */
        DB::table('page')->insert([
            'lang' => 'ua',
            'link' => 'offers',
            'title' => 'Навички',
            'name' => 'Ви знаєте, що ми можемо?<br/>Що ми робимо?<br/>Що ми підтримуємо?',
            'description' => 'Наші можливості обмежені лише нашою уявою.',
        ]);

        DB::table('page')->insert([
            'lang' => 'ru',
            'link' => 'offers',
            'title' => 'Навыки',
            'name' => 'Ты знаешь что мы можем?<br/>Что мы делаем?<br/>Что мы поддерживаем?',
            'description' => 'Наши возможности ограничены только нашим воображением.',
        ]);

        DB::table('page')->insert([
            'lang' => 'en',
            'link' => 'offers',
            'title' => 'We Offers',
            'name' => 'Do you know what we can?<br/>What we do?<br/>What we support?',
            'description' => 'Our possibilities are limited only by our imagination.',
        ]);


        /* Empolyee page */
        DB::table('page')->insert([
            'lang' => 'ua',
            'link' => 'our-staff',
            'title' => 'Персонал',
            'name' => 'Ті люди, які можуть легко реалізувати все те, що ви хочете.',
            'description' => 'Разом ми можемо більше, ніж кожен окремо.',
        ]);

        DB::table('page')->insert([
            'lang' => 'ru',
            'link' => 'our-staff',
            'title' => 'Персонал',
            'name' => 'Те люди, которые могут легко реализовать все то, что вы хотите.?',
            'description' => 'Вместе мы можем больше, чем в одиночку.',
        ]);

        DB::table('page')->insert([
            'lang' => 'en',
            'link' => 'our-staff',
            'title' => 'Our Staff',
            'name' => 'Many people who can easily realize all things which you wish.',
            'description' => 'Together we can more than one by one.',
        ]);


        /* Portfolio page */
        DB::table('page')->insert([
            'lang' => 'ua',
            'link' => 'portfolio',
            'title' => 'Портфоліо',
            'name' => 'Наші проекти, якими ми пишаємось.',
            'description' => 'Плоди нашої діяльності.',
        ]);

        DB::table('page')->insert([
            'lang' => 'ru',
            'link' => 'portfolio',
            'title' => 'Портфолио',
            'name' => 'Наши проекты которыми мы гордимся.',
            'description' => 'Плоды нашей скромной деятельности.',
        ]);

        DB::table('page')->insert([
            'lang' => 'en',
            'link' => 'portfolio',
            'title' => 'Portfolio',
            'name' => 'Our implemented incredible projects which we are proud of.',
            'description' => 'The fruits of our humble activity..',
        ]);

    }
}
