<?php

use Illuminate\Database\Seeder;

class CoverTableSeederAft extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* About page */
        DB::table('cover')->insert([
            'image' => 'storage/default/images/about.jpg',
            'video' => 'storage/default/videos/about-us-pc.mp4',
            'page_id' => 1
        ]);

        DB::table('cover')->insert([
            'image' => 'storage/default/images/about.jpg',
            'video' => 'storage/default/videos/about-us-pc.mp4',
            'page_id' => 2
        ]);

        DB::table('cover')->insert([
            'image' => 'storage/default/images/about.jpg',
            'video' => 'storage/default/videos/about-us-pc.mp4',
            'page_id' => 3
        ]);

        /* Offers page */
        DB::table('cover')->insert([
            'image' => 'storage/default/images/offers.jpg',
            'video' => 'storage/default/videos/we-offer-pc.mp4',
            'page_id' => 4
        ]);

            DB::table('cover')->insert([
                'image' => 'storage/default/images/offers.jpg',
                'content_id' => 1
            ]);
            DB::table('cover')->insert([
                'image' => 'storage/default/images/offers.jpg',
                'content_id' => 2
            ]);
            DB::table('cover')->insert([
                'image' => 'storage/default/images/offers.jpg',
                'content_id' => 3
            ]);

        DB::table('cover')->insert([
            'image' => 'storage/default/images/offers.jpg',
            'video' => 'storage/default/videos/we-offer-pc.mp4',
            'page_id' => 5
        ]);

            DB::table('cover')->insert([
                'image' => 'storage/default/images/offers.jpg',
                'content_id' => 4
            ]);
            DB::table('cover')->insert([
                'image' => 'storage/default/images/offers.jpg',
                'content_id' => 5
            ]);
            DB::table('cover')->insert([
                'image' => 'storage/default/images/offers.jpg',
                'content_id' => 6
            ]);

        DB::table('cover')->insert([
            'image' => 'storage/default/images/offers.jpg',
            'video' => 'storage/default/videos/we-offer-pc.mp4',
            'page_id' => 6
        ]);

            DB::table('cover')->insert([
                'image' => 'storage/default/images/offers.jpg',
                'content_id' => 7
            ]);
            DB::table('cover')->insert([
                'image' => 'storage/default/images/offers.jpg',
                'content_id' => 8
            ]);
            DB::table('cover')->insert([
                'image' => 'storage/default/images/offers.jpg',
                'content_id' => 9
            ]);

        /* Empolyee page */
        DB::table('cover')->insert([
            'image' => 'storage/default/images/our-staff.jpg',
			'video' => 'storage/default/videos/staff-pc.mp4',
            'page_id' => 7
        ]);

            DB::table('cover')->insert([
                'image' => 'storage/default/images/offers.jpg',
                'content_id' => 10
            ]);

        DB::table('cover')->insert([
            'image' => 'storage/default/images/our-staff.jpg',
            'video' => 'storage/default/videos/staff-pc.mp4',
            'page_id' => 8
        ]);

            DB::table('cover')->insert([
                'image' => 'storage/default/images/offers.jpg',
                'content_id' => 11
            ]);

        DB::table('cover')->insert([
            'image' => 'storage/default/images/our-staff.jpg',
            'video' => 'storage/default/videos/staff-pc.mp4',
            'page_id' => 9
        ]);

            DB::table('cover')->insert([
                'image' => 'storage/default/images/offers.jpg',
                'content_id' => 12
            ]);

        /* Portfolio page */
        DB::table('cover')->insert([
            'image' => 'storage/default/images/portfolio.jpg',
			'video' => 'storage/default/videos/portfolio-pc.mp4',
            'page_id' => 10
        ]);

            DB::table('cover')->insert([
                'image' => 'storage/default/images/portfolio.jpg',
                'content_id' => 13
            ]);
            DB::table('cover')->insert([
                'image' => 'storage/default/images/portfolio.jpg',
                'content_id' => 14
            ]);
            DB::table('cover')->insert([
                'image' => 'storage/default/images/portfolio.jpg',
                'content_id' => 15
            ]);

        DB::table('cover')->insert([
            'image' => 'storage/default/images/portfolio.jpg',
            'video' => 'storage/default/videos/portfolio-pc.mp4',
            'page_id' => 11
        ]);

            DB::table('cover')->insert([
                'image' => 'storage/default/images/avatar.jpg',
                'content_id' => 16
            ]);
            DB::table('cover')->insert([
                'image' => 'storage/default/images/avatar.jpg',
                'content_id' => 17
            ]);
            DB::table('cover')->insert([
                'image' => 'storage/default/images/avatar.jpg',
                'content_id' => 18
            ]);

        DB::table('cover')->insert([
            'image' => 'storage/default/images/portfolio.jpg',
            'video' => 'storage/default/videos/portfolio-pc.mp4',
            'page_id' => 12
        ]);

            DB::table('cover')->insert([
                'image' => 'storage/default/images/SOC.jpg',
                'content_id' => 19
            ]);
            DB::table('cover')->insert([
                'image' => 'storage/default/images/JOB.jpg',
                'content_id' => 20
            ]);
            DB::table('cover')->insert([
                'image' => 'storage/default/images/AC.jpg',
                'content_id' => 21
            ]);

            DB::table('cover')->insert([
                'image' => 'storage/default/images/SOC.jpg',
                'content_id' => 22
            ]);
            DB::table('cover')->insert([
                'image' => 'storage/default/images/JOB.jpg',
                'content_id' => 23
            ]);
            DB::table('cover')->insert([
                'image' => 'storage/default/images/AC.jpg',
                'content_id' => 24
            ]);

            DB::table('cover')->insert([
                'image' => 'storage/default/images/SOC.jpg',
                'content_id' => 25
            ]);
            DB::table('cover')->insert([
                'image' => 'storage/default/images/JOB.jpg',
                'content_id' => 26
            ]);
            DB::table('cover')->insert([
                'image' => 'storage/default/images/AC.jpg',
                'content_id' => 27
            ]);
    }
}
